import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import { useEffect, useState } from 'react';

import { AudioInput, AudioInputConfiguration } from 'cordova-plugin-audioinput';
declare var audioinput: AudioInput;

const Tab1: React.FC = () => {

  const [audioState, setAudioState] = useState(['init'])
  const appendAudio = (state: any) => setAudioState([...audioState, state])

  const startCapture = async () => {
    audioinput.start({
      bufferSize: 4096 * 2,
      sampleRate: 16000,
      channels: 1,
      format: audioinput.FORMAT.PCM_16BIT,
    });
    appendAudio('started')
    console.log('started')
  };

  const stopCapture = async () => {
    audioinput.stop(() => {
      appendAudio('stopped')
      console.log('stopped')
    })
  };

  const handleAudioEvent = (evt: any) => {
    appendAudio(evt.data.length)
    console.log(evt.data.length)
    // evt.data contains raw pcm data
  }
  
  const handleAudioError = (err: any) => {
    appendAudio("err: " + JSON.stringify(err))
  }

  useEffect(() => {
    // Check if we already have permission to record
    audioinput.checkMicrophonePermission(function (hasPermission: boolean) {
      if (hasPermission) {
        console.log("We already have permission to record.");
      } else {
        // Ask the user for permission to access the microphone
        audioinput.getMicrophonePermission(function (
          hasPermission: boolean,
          message: string
        ) {
          if (hasPermission) {
            console.log("User granted us permission to record.");
          } else {
            console.warn("User denied permission to record.");
          }
        });
      }
    });
    window.addEventListener("audioinput", handleAudioEvent);
    window.addEventListener("audioinputerror", handleAudioError);
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Tab 1 page" />
        <button onClick={startCapture}>start</button>{' '}
        <button onClick={stopCapture}>stop</button>
        <ol>
          {audioState.map(i => {
            return <li>{i}</li>
          })}
        </ol>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
